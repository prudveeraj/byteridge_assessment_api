﻿const config = require('config.js');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const db = require('_helpers/db');
const User = db.User;

module.exports = {
    authenticate,
    getAll,
    getById,
    create,
    update,
    delete: _delete,
    logout,
    audit
};

async function audit() {                            //serves when auditor request comes
    return await User.find().select('-hash');
}

async function logout({ username }) {                //storing for user logoutTime
    const user = await User.findOne({ username });
    if (user) {     
        if(!user.logout_history) { 
            let logout_history = []; 
            logout_history.push(Date.now());
            user.logout_history = login_history;
            } else user.logout_history.push(Date.now()); 
        update(user.id, user) ;       
        return {
            logout : "success"
        };
    }
}

async function authenticate({ username, password, clientIp }) {
    const user = await User.findOne({ username });
    if (user && bcrypt.compareSync(password, user.hash)) {  
        if(!user.login_history) { 
        let login_history = []; 
        login_history.push(Date.now());
        user.login_history = login_history;
        } else user.login_history.push(Date.now());       
        user.ip = clientIp;
        update(user.id, user)
        const { hash, ...userWithoutHash } = user.toObject();
        const token = jwt.sign({ sub: user.id, role: user.role }, config.secret);
        // console.log("userwithoush hash", userWithoutHash)
        return {
            ...userWithoutHash,
            token
        };
    }
}

async function getAll() {
    return await User.find().select('-hash');
}

async function getById(id) {
    return await User.findById(id).select('-hash');
}

async function create(userParam) {
    // validate
    if (await User.findOne({ username: userParam.username })) {
        throw 'Username "' + userParam.username + '" is already taken';
    }

    const user = new User(userParam);

    // hash password
    if (userParam.password) {
        user.hash = bcrypt.hashSync(userParam.password, 10);
    }

    // save user
    await user.save();
}

async function update(id, userParam) {
    const user = await User.findById(id);
    
    // validate
    if (!user) throw 'User not found';
    if (user.username !== userParam.username && await User.findOne({ username: userParam.username })) {
        throw 'Username "' + userParam.username + '" is already taken';
    }

    // hash password if it was entered
    if (userParam.password) {
        userParam.hash = bcrypt.hashSync(userParam.password, 10);
    }

    // copy userParam properties to user
    Object.assign(user, userParam);

    await user.save();
}

async function _delete(id) {
    await User.findByIdAndRemove(id);
}