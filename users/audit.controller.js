﻿const express = require('express');
const router = express.Router();
const userService = require('./user.service');
const jwt = require('jsonwebtoken');
const config = require('config.js');

router.get('/', audit); //serves when /audit request comes

module.exports = router;

function audit(req, res, next) {
    let user = jwt.verify(req.headers['token'],config.secret);
    if(!user.role) { res.status(401).json({ message: 'Invalid Token', status: 'UNAUTHORIZED-REQUEST' }); }   //sends 401 reponse when unAuthorized /audit occurs
    else { 
        userService.audit()
        .then(users => res.json(users))
        .catch(err => next(err));
    }
}